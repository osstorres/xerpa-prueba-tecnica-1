"""
Unit testing
"""
import unittest

from src.functions import (
    divided_by,
    eight,
    five,
    four,
    minus,
    nine,
    one,
    plus,
    seven,
    three,
    times,
)


class TestFunctions(unittest.TestCase):
    def test_20(self):
        assert four(times(five())) == 20

    def test_9(self):
        assert one(plus(eight())) == 9

    def test_4(self):
        assert seven(minus(three())) == 4

    def test_3(self):
        assert nine(divided_by(three())) == 3

    def test_3_fail(self):
        assert nine(divided_by(three())) != -2222

    def test_20_fail(self):
        assert four(times(five())) != "20"

    def test_9_fail(self):
        assert one(plus(eight())) != -9

    def test_4_fail(self):
        assert seven(minus(three())) != -4


if __name__ == "__main__":
    unittest.main()
