def zero(f=None):
    return 0 if not f else f(0)


def one(f=None):
    return 1 if not f else f(1)


def two(f=None):
    return 2 if not f else f(2)


def three(f=None):
    return 3 if not f else f(3)


def four(f=None):
    return 4 if not f else f(4)


def five(f=None):
    return 5 if not f else f(5)


def six(f=None):
    return 6 if not f else f(6)


def seven(f=None):
    return 7 if not f else f(7)


def eight(f=None):
    return 8 if not f else f(8)


def nine(f=None):
    return 9 if not f else f(9)


def plus(y):
    """
    Plus(y) returns a function that adds y to its argument.

    :param y: the number to be added, subtracted, multiplied, or divided by
    :return: A function that takes a single argument
    and returns the result of the operation.
    """
    def add(x):
        return x + y

    return add


def minus(y):
    """
    Minus returns a function that takes an argument x and returns x minus y.

    :param y: the number to be subtracted from the number x
    :return: A function
    """
    def substract(x):
        return x - y

    return substract


def times(y):
    """
    Times returns a function that multiplies its argument by y.

    :param y: the parameter that is passed into the outer function
    :return: A function
    """
    def multiply(x):
        return x * y

    return multiply


def divided_by(y):
    """
    Divided_by returns a function that divides its argument by y.

    :param y: the number to divide by
    :return: A function that divides the input by
    the argument passed to the outer function.
    """
    def divide(x):
        return x // y

    return divide
